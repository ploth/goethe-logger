#!/usr/bin/python3
import re
from pathlib import Path
from datetime import datetime

# TODO quick cheat sheet how too book (argument)
# TODO have an identifier for an event an a extra description
# TODO better printing - to views at the same time for combination 1h2m and 62m
# TODO better printing - do not print on one line, group by identifier
# TODO current workday "stopwatch" (print with fake finish now) (make it useable for awesome wm)
# TODO current workday "countdown" other way around (respect pause)

class Event():
    time: int
    duration: int
    description: str
    category: str

    def __repr__(self):
        return f'{self.time}-{self.time+self.duration} ({self.duration}): {self.category} ({self.description})'


def parse_time(h: str, m: str) -> int:
    return int(h) * 60 + int(m)


def time_to_string(t: int) -> str:
    h = t//60
    return f'{h}:{t-h*60:02d}'


aliases = {
    'pause': ['mittag', 'essen'],
    'emerging design': ['review', 'emerge'],
    'iteration planning': ['ip', 'subteam', 'commitment', 'wbk', 'ipmeet'],
    'task breakdown': ['tb', 'breakdown'],
    'product ownership': ['mails', 'po', 'standup']
}


def resolve_aliases(events: dict) -> dict:
    for k, v in aliases.items():
        for x in v:
            if x in events:
                events[k] = events.get(k, 0) + events[x]
                del events[x]


def parse_and_print(file):
    with open(file) as f:
        lines = f.readlines()
        parsed = []
        for line in lines:
            # Leave out new line character
            match = re.search(r'^(?P<h>\d+):(?P<m>\d+)\s(?P<description>.*)', line[:-1])
            # You can overwrite a category afterwards by putting it in []
            override_desc = re.search(r'(?<=\[).+(?=\])', line)
            if override_desc:
                category = override_desc.group(0).strip()
            else:
                category = match.group('description').strip()
            e = Event()
            e.time = parse_time(match.group('h'), match.group('m'))
            e.description = match.group('description')
            e.category = category.lower()
            parsed.append(e)
    for i, e in enumerate(parsed):
        if i<len(lines)-1:
            e.duration = parsed[i+1].time - e.time
        else:
            e.duration = 0

    parsed.sort(key=lambda x: x.category)

    categories = set([x.category for x in parsed])
    time_taken = dict()
    for key in categories:
        time_taken[key] = 0
    for e in parsed:
        time_taken[e.category] += e.duration

    resolve_aliases(time_taken)
    todays_sum = sum(time_taken.values()) - time_taken.get('pause', 0)
    if todays_sum != 0:
        print(time_to_string(todays_sum))
        time_taken = {k: v for k, v in time_taken.items() if v}
        for k, v in time_taken.items():
            time_taken[k] = time_to_string(v)
        print(time_taken)


def parse_args():
    import argparse

    arg_parser = argparse.ArgumentParser(description="Progress log parser for goethe.ai booking.",
                                         formatter_class=argparse.RawTextHelpFormatter)
    arg_parser.add_argument("-f", "--file", type=str, help="use specified log file (default: today)")
    arg_parser.add_argument('-p', '--print', action='store_true', help="print log")
    arg_parser.add_argument('-c', '--close-today', action='store_true', help="finish todays log")
    args = arg_parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    file = "today"
    print_log = args.print
    cwd = Path.cwd()

    if args.file:
        file = args.file

    if print_log:
        parse_and_print(file)

    if args.close_today:
        date_string = datetime.today().strftime('%Y-%m-%d')
        today_date_file_path = cwd/date_string
        if today_date_file_path.exists():
            print("Closed already!")
        else:
            today_file_path = cwd/"today"
            today_file_path.rename(date_string)
            today_file_path.touch(exist_ok=False)

